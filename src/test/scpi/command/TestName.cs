﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestName
	{
		[Test]
		public void CanBeQueried()
		{
			using var stream = new MemoryStream();

			try
			{
				new GetName(stream).Execute();
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($"*IDN?", query);
		}
		
		[Test]
		public void CanBeParsed()
		{
			const string expectedName = "PACE6000";
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($"*IDN GE Druck,{expectedName},3506439,DK0388  v01.01.33");
			writer.Flush();
			stream.Position = 0;
			
			var readNeme = new GetName(new ReadOnlyStream(stream))
				.Execute();
			
			Assert.AreEqual(expectedName, readNeme);
		}
	}
}
