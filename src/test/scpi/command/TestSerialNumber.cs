﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestSerialNumber
	{
		[Test]
		public void CanBeQueried()
		{
			using var stream = new MemoryStream();

			try
			{
				new GetSerialNumber(stream).Execute();
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":INST:SN?", query);
		}
		
		[Test]
		public void CanBeParsed()
		{
			const string expectedSerialNumber = "3506439";
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":INST:SN {expectedSerialNumber}");
			writer.Flush();
			stream.Position = 0;
			
			var readSerialNumber = new GetSerialNumber(new ReadOnlyStream(stream))
				.Execute();
			
			Assert.AreEqual(expectedSerialNumber, readSerialNumber);
		}
	}
}
