// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using imperfect.devices.pace.model;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{

	public class TestMaximumPressure
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new GetMaximumPressure(stream).Execute(module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":INSTrument:SENSe{(int) module}:FULLscale?", query);
		}
		
		[Test]
		public void CanBeParsed()
		{
			const int expectedMaximum = 10000;
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":INST:CONT:SENS:FULL {expectedMaximum}.0000000, -1000.0000000");
			writer.Flush();
			stream.Position = 0;
			
			var readMaximum = new GetMaximumPressure(new ReadOnlyStream(stream))
				.Execute(Module.First);
			
			Assert.AreEqual(expectedMaximum, readMaximum);
		}
	}
}
