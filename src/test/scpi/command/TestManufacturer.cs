﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestManufacturer
	{
		[Test]
		public void CanBeQueried()
		{
			using var stream = new MemoryStream();

			try
			{
				new GetManufacturer(stream).Execute();
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($"*IDN?", query);
		}
		
		[Test]
		public void CanBeParsed()
		{
			const string expectedManufacturer = "GE Druck";
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($"*IDN {expectedManufacturer},PACE6000,3506439,DK0388  v01.01.33");
			writer.Flush();
			stream.Position = 0;
			
			var readManufacturer = new GetManufacturer(new ReadOnlyStream(stream))
				.Execute();
			
			Assert.AreEqual(expectedManufacturer, readManufacturer);
		}
	}
}
