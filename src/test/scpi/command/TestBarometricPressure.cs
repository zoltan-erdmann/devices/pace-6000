﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Globalization;
using System.IO;
using System.Text;
using NUnit.Framework;
using imperfect.devices.pace.model;

namespace imperfect.devices.pace.scpi.command
{
	public class TestBarometricPressure
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();
			
			try
			{
				new GetBarometricPressure(stream).Execute(module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":SENSe{(int) module}:PRESsure:BARometer?", query);
		}
		
		[Test]
		public void CanBeParsed()
		{
			const Module module = Module.First;
			const float expectedPressure = 1014.9324244f;
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":SENS:PRES:BAR {expectedPressure.ToString(CultureInfo.InvariantCulture)}");
			writer.Flush();
			stream.Position = 0;
			
			var readPressure = new GetBarometricPressure(new ReadOnlyStream(stream))
				.Execute(module);
			
			Assert.AreEqual(expectedPressure, readPressure);
		}
	}
}
