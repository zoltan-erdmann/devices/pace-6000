// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using imperfect.devices.pace.model;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestVenting
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new GetVentingStatus(stream).Execute(module);
			}
			catch (Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine() ?? "";

			Assert.AreEqual($":SOUR1:PRES:LEV:IMM:AMPL:VENT?", query);
		}

		[Test]
		[TestCase((int) VentingStatus.Ok)]
		[TestCase((int) VentingStatus.InProgress)]
		[TestCase((int) VentingStatus.Completed)]
		public void CanBeParsed(int expectedStatus)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":SOUR:PRES:LEV:IMM:AMPL:VENT {expectedStatus}");
			writer.Flush();
			stream.Position = 0;

			var readStatus = new GetVentingStatus(new ReadOnlyStream(stream))
				.Execute(module);

			Assert.AreEqual(expectedStatus, (int) readStatus);
		}

		[Test]
		[TestCase((int)VentingAction.Abort)]
		[TestCase((int)VentingAction.Start)]
		public void CanBeSet(int expectedAction)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new SetVentingStatus(stream).Execute((VentingAction)expectedAction, module);
			}
			catch (Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var valueSet = reader.ReadLine() ?? "";

			Assert.AreEqual($":SOUR{(int)module}:PRES:LEV:IMM:AMPL:VENT {expectedAction}", valueSet);
		}
	}
}
