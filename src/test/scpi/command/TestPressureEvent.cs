// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestPressureEvent
	{
		[Test]
		public void CanBeQueried()
		{
			using var stream = new MemoryStream();

			try
			{
				new GetPressureEvent(stream).Execute();
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":STATus:OPERation:PRESsure:EVENt?", query);
		}
		
		[Test]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(4)]
		[TestCase(30)]
		[TestCase(35)]
		public void CanBeParsed(int expectedFlags)
		{
			bool expectedVentingEventActive = (expectedFlags & 1) != 0;
			bool expectedPressureEventActive = (expectedFlags & 4) != 0;
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":STAT:OPER:PRES:EVEN {expectedFlags}");
			writer.Flush();
			stream.Position = 0;
			
			var readEvent = new GetPressureEvent(new ReadOnlyStream(stream))
				.Execute();

			Assert.AreEqual(expectedVentingEventActive, readEvent.VentingComplete);
			Assert.AreEqual(expectedPressureEventActive, readEvent.IsPressureInRange);
		}
	}
}
