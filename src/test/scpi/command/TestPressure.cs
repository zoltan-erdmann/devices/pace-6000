// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Globalization;
using System.IO;
using System.Text;
using imperfect.devices.pace.model;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestPressure
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new GetPressure(stream).Execute(module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":SENSe{(int) module}:PRESsure?", query);
		}
		
		[Test]
		[TestCase(9007.2862419f)]
		[TestCase(7012.8251051f)]
		public void CanBeParsed(float expectedPressure)
		{
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":SENSe:PRESsure {expectedPressure.ToString(CultureInfo.InvariantCulture)}");
			writer.Flush();
			stream.Position = 0;
			
			var readMaximum = new GetPressure(new ReadOnlyStream(stream))
				.Execute(Module.First);
			
			Assert.AreEqual(expectedPressure, readMaximum);
		}

		[Test]
		public void CanBeSet()
		{
			const Module module = Module.First;
			const float expectedPressure = 2400;
			using var stream = new MemoryStream();

			try
			{
				new SetPressure(stream).Execute(expectedPressure, module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var pressureValueSet = reader.ReadLine()?? "";
			
			Assert.AreEqual($":SOURce{(int) module}:PRESsure:LEVel {expectedPressure}", pressureValueSet);
		}
	}
}
