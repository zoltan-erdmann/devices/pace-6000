﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestModeToLocal
	{
		
		[Test]
		public void CanBeSet()
		{
			using var stream = new MemoryStream();

			try
			{
				new SetModeToLocal(stream).Execute();
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var valueSet = reader.ReadLine()?? "";
			
			Assert.AreEqual($":LOC", valueSet);
		}
	}
}
