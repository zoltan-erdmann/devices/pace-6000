﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using imperfect.devices.pace.model;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestUnit
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new GetUnit(stream).Execute(module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":UNIT1:PRES?", query);
		}
		
		[Test]
		[TestCase(Unit.Bar, "BAR")]
		[TestCase(Unit.Pascal, "PA")]
		[TestCase(Unit.MilliBar, "MBAR")]
		public void CanBeParsed(Unit expectedUnit, string unitString)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":UNIT:PRES {unitString}");
			writer.Flush();
			stream.Position = 0;
			
			var readUnit = new GetUnit(new ReadOnlyStream(stream))
				.Execute(module);
			
			Assert.AreEqual(expectedUnit, readUnit);
		}
		
		[Test]
		[TestCase(Unit.Bar, "BAR")]
		[TestCase(Unit.Pascal, "PA")]
		[TestCase(Unit.MilliBar, "MBAR")]
		public void CanBeSet(Unit expectedUnit, string unitString)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new SetUnit(stream).Execute(expectedUnit, module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var valueSet = reader.ReadLine()?? "";
			
			Assert.AreEqual($":UNIT{(int) module}:PRES {unitString}", valueSet);
		}
	}
}
