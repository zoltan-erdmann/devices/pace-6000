﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Text;
using imperfect.devices.pace.model;
using NUnit.Framework;

namespace imperfect.devices.pace.scpi.command
{
	public class TestOutputValveState
	{
		[Test]
		public void CanBeQueried()
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new GetOutputValveState(stream).Execute(module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var query = reader.ReadLine()?? "";
			
			Assert.AreEqual($":OUTPut{(int) module}:ISOLation:STATe?", query);
		}
		
		[Test]
		[TestCase(State.Off)]
		[TestCase(State.On)]
		public void CanBeParsed(State expectedState)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();
			
			var writer = new StreamWriter(stream, Encoding.ASCII);
			writer.WriteLine($":OUTP:ISOL:STAT {(int) expectedState}");
			writer.Flush();
			stream.Position = 0;
			
			var readState = new GetOutputValveState(new ReadOnlyStream(stream))
				.Execute(module);
			
			Assert.AreEqual(expectedState, readState);
		}
		
		[Test]
		[TestCase(State.Off)]
		[TestCase(State.On)]
		public void CanBeSet(State expectedState)
		{
			const Module module = Module.First;
			using var stream = new MemoryStream();

			try
			{
				new SetOutputValveState(stream).Execute(expectedState, module);
			}
			catch(Exception)
			{
				// Intentional SKIP
			}

			stream.Position = 0;
			var reader = new StreamReader(stream, Encoding.ASCII);
			var valueSet = reader.ReadLine()?? "";
			
			Assert.AreEqual($":OUTPut{(int) module}:ISOLation:STATe {(int) expectedState}", valueSet);
		}
	}
}
