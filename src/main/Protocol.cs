// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;

namespace imperfect.devices.pace
{
	public interface Protocol
	{
		public float GetAbsolutePressure(Module module);
		
		public float GetBarometricPressure(Module module);

		public string GetManufacturer();
		
		public float GetMaximumPressure(Module module);

		public float GetMinimumPressure(Module module);

		public string GetName();

		public State GetOutputState(Module module);

		public State GetOutputValveState(Module module);

		public float GetPressure(Module module);

		public PressureEvent GetPressureEvent();

		public string GetSerialNumber();
		
		public Unit GetUnit(Module module);

		public VentingStatus GetVentingStatus(Module module);

		public void SetModeToLocal();

		public void SetOutputState(State state, Module module);

		public void SetOutputValveState(State state, Module module);

		public void SetPressure(float pressure, Module module);

		public void SetUnit(Unit unit, Module module);

		public void StartVenting(Module module);

		public void StopVenting(Module module);
	}
}
