// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	public record PressureEvent
	{
		public bool VentingComplete { get; init; }
		public bool IsPressureInRange { get; init; }
	}
}
