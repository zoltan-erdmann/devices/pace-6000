// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	public enum VentingStatus
	{
		Ok = 0,
		InProgress = 1,
		Completed = 2
	}
}
