﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	public enum State
	{
		Off = 0,
		On = 1
	}
}
