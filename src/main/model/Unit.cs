﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	public enum Unit
	{
		MilliBar = 1,
		Bar = 2,
		Pascal = 3
	}
}
