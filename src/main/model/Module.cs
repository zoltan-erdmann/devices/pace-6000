﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	public enum Module
	{
		First = 1,
		Second = 2
	}
}
