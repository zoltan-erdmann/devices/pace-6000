// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.model
{
	internal enum VentingAction
	{
		Abort = 0,
		Start = 1
	}
}
