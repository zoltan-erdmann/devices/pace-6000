// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;

namespace imperfect.devices.pace
{
	public class Pace6000
	{
		private readonly Protocol protocol;
		
		public Pace6000(Protocol protocol)
		{
			this.protocol = protocol;
		}

		public float GetBarometricPressure(Module module = Module.First) =>
			protocol.GetBarometricPressure(module);

		public string GetManufacturer() =>
			protocol.GetManufacturer();
		
		public float GetMaximumPressure(Module module = Module.First) =>
			protocol.GetMaximumPressure(module);

		public float GetMinimumPressure(Module module = Module.First) =>
			protocol.GetMinimumPressure(module);

		public string GetName() =>
			protocol.GetName();

		public State GetOutputState(Module module) =>
			protocol.GetOutputState(module);

		public State GetOutputValveState(Module module = Module.First) =>
			protocol.GetOutputValveState(module);
		
		public float GetPressure(Module module = Module.First) =>
			protocol.GetPressure(module);

		public PressureEvent GetPressureEvent() =>
			protocol.GetPressureEvent();

		public string GetSerialNumber() =>
			protocol.GetSerialNumber();

		public Unit GetUnit(Module module = Module.First) =>
			protocol.GetUnit(module);

		public VentingStatus GetVentingStatus(Module module = Module.First) =>
			protocol.GetVentingStatus(module);

		public void SetModeToLocal() =>
			protocol.SetModeToLocal();

		public void SetOutputState(State state, Module module = Module.First) =>
			protocol.SetOutputState(state, module);

		public void SetOutputValveState(State state, Module module = Module.First) =>
			protocol.SetOutputValveState(state, module);
		
		public void SetPressure(float pressure, Module module = Module.First) =>
			protocol.SetPressure(pressure, module);

		public void SetUnit(Unit unit, Module module = Module.First) =>
			protocol.SetUnit(unit, module);

		public void StartVenting(Module module = Module.First) =>
			protocol.StartVenting(module);

		public void StopVenting(Module module = Module.First) =>
			protocol.StopVenting(module);
	}
}
