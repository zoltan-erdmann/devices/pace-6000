﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.pace.scpi
{
	internal static class Constants
	{
		public const string Terminator = "\n";
		public const int ReadBufferSize = 128;
	}
}
