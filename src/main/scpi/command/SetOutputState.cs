// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;
using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class SetOutputState
	{
		private readonly Stream stream;

		public SetOutputState(Stream stream)
		{
			this.stream = stream;
		}

		public void Execute(State state, Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":OUTPut{(int)module}:STATe {(int)state}"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
		}
	}
}
