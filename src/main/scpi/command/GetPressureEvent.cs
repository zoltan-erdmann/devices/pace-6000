// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetPressureEvent
	{
		private readonly Stream stream;
		
		public GetPressureEvent(Stream stream)
		{
			this.stream = stream;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>The barometric pressure in millibar.</returns>
		public PressureEvent Execute()
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":STATus:OPERation:PRESsure:EVENt?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
			
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return ToPressureEvent(int.Parse(response.SkipWhile(x => x != ' ').ToArray()));
		}

		private PressureEvent ToPressureEvent(int eventRegister) => new PressureEvent()
		{
			VentingComplete = (eventRegister & 0x01) != 0,
			IsPressureInRange = (eventRegister & 0x04) != 0
		};
	}
}
