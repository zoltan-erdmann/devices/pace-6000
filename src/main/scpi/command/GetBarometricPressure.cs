﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetBarometricPressure
	{
		private readonly Stream stream;
		
		public GetBarometricPressure(Stream stream)
		{
			this.stream = stream;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="module">Module to use</param>
		/// <returns>The barometric pressure in millibar.</returns>
		public float Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":SENSe{(int) module}:PRESsure:BARometer?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
			
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return float.Parse(string.Concat(
					response.SkipWhile(x => x != ' ').ToArray()), CultureInfo.InvariantCulture);
		}
	}
}
