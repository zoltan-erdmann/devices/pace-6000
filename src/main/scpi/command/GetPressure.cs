// SPDX-License-Identifier: LGPL-3.0-only

using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetPressure
	{
		private readonly Stream stream;
			
		public GetPressure(Stream stream)
		{
			this.stream = stream;
		}
			
		public float Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":SENSe{(int) module}:PRESsure?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
				
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return float.Parse(string.Concat(
				response.SkipWhile(x => x != ' ').ToArray()), CultureInfo.InvariantCulture);
		}
	}
}
