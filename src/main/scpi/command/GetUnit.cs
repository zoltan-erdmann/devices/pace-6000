﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetUnit
	{
		private readonly Stream stream;
		
		public GetUnit(Stream stream)
		{
			this.stream = stream;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="module">Module to use</param>
		/// <returns>The barometric pressure in millibar.</returns>
		public Unit Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":UNIT{(int) module}:PRES?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
			
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return ParseUnit(string.Concat(response.SkipWhile(x => x != ' ').ToArray()).Trim());
		}

		/// <summary>
		/// Only a couple of units are supported at the moment.
		/// </summary>
		/// <returns>Unit parsed from the string.</returns>
		private Unit ParseUnit(string unit)
		{
			var upperCasedUnit = unit.ToUpper();
			return upperCasedUnit switch
			{
				"BAR" => Unit.Bar,
				"MBAR" => Unit.MilliBar,
				"PA" => Unit.Pascal,
				_ => throw new ArgumentOutOfRangeException(nameof(unit))
			};
		}
	}
}
