﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetOutputValveState
	{
		private readonly Stream stream;
		
		public GetOutputValveState(Stream stream)
		{
			this.stream = stream;
		}
		
		public State Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":OUTPut{(int) module}:ISOLation:STATe?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
			
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return (State) int.Parse(response.SkipWhile(x => x != ' ').ToArray());
		}
	}
}
