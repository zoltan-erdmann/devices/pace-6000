﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class SetUnit
	{
		private readonly Stream stream;
		
		public SetUnit(Stream stream)
		{
			this.stream = stream;
		}
		
		public void Execute(Unit unit, Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":UNIT{(int) module}:PRES {ToUnitName(unit)}"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
		}

		private string ToUnitName(Unit unit) =>
			unit switch
			{
				Unit.Bar => "BAR", 
				Unit.MilliBar => "MBAR",
				Unit.Pascal => "PA",
			_ => throw new ArgumentOutOfRangeException(nameof(unit))
		};
	}
}
