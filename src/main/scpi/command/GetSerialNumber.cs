﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetSerialNumber
	{
		private readonly Stream stream;
		
		public GetSerialNumber(Stream stream)
		{
			this.stream = stream;
		}
		
		public string Execute()
		{
			byte[] bytes = Encoding.ASCII.GetBytes(":INST:SN?".Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
			
			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return new string(response.SkipWhile(x => x != ' ').ToArray()).Trim();
		}
	}
}
