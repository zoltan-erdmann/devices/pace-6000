﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	public class SetModeToLocal
	{
		private readonly Stream stream;
		
		public SetModeToLocal(Stream stream)
		{
			this.stream = stream;
		}
		
		public void Execute()
		{
			byte[] bytes = Encoding.ASCII.GetBytes(":LOC"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
		}
	}
}
