// SPDX-License-Identifier: LGPL-3.0-only

using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class SetPressure
	{
		private readonly Stream stream;
		
		public SetPressure(Stream stream)
		{
			this.stream = stream;
		}
		
		public void Execute(float pressure, Module module)
		{
			var pressureAsString = pressure.ToString(CultureInfo.InvariantCulture);
			byte[] bytes = Encoding.ASCII.GetBytes($":SOURce{(int)module}:PRESsure:LEVel {pressureAsString}"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
		}
	}
}
