// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;
using System;
using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetVentingStatus
	{
		private readonly Stream stream;

		public GetVentingStatus(Stream stream)
		{
			this.stream = stream;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="module">Module to use</param>
		/// <returns>The status of venting.</returns>
		public VentingStatus Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":SOUR{(int)module}:PRES:LEV:IMM:AMPL:VENT?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);

			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return ParseStatus(string.Concat(response.SkipWhile(x => x != ' ').ToArray()).Trim());
		}

		private VentingStatus ParseStatus(string status)
		{
			return status switch
			{
				"0" => VentingStatus.Ok,
				"1" => VentingStatus.InProgress,
				"2" => VentingStatus.Completed,
				_ => throw new ArgumentOutOfRangeException(nameof(status))
			};
		}
	}
}
