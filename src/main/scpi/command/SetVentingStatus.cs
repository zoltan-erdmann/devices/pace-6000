// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;
using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class SetVentingStatus
	{
		private readonly Stream stream;

		public SetVentingStatus(Stream stream)
		{
			this.stream = stream;
		}

		public void Execute(VentingAction action, Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":SOUR{(int)module}:PRES:LEV:IMM:AMPL:VENT {(int)action}"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);
		}
	}
}
