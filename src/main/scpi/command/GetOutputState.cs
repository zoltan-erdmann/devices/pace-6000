// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.pace.model;
using System.IO;
using System.Linq;
using System.Text;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi.command
{
	internal class GetOutputState
	{
		private readonly Stream stream;

		public GetOutputState(Stream stream)
		{
			this.stream = stream;
		}

		public State Execute(Module module)
		{
			byte[] bytes = Encoding.ASCII.GetBytes($":OUTPut{(int)module}:STATe?"
				.Concat(Terminator).ToArray());
			stream.Write(bytes, 0, bytes.Length);

			var reader = new StreamReader(stream, Encoding.ASCII);
			var response = reader.ReadLine() ?? "";

			return (State)int.Parse(response.SkipWhile(x => x != ' ').ToArray());
		}
	}
}
