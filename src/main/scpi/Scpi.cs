// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using System.Text;
using imperfect.devices.pace.model;
using imperfect.devices.pace.scpi.command;

using static imperfect.devices.pace.scpi.Constants;

namespace imperfect.devices.pace.scpi
{
	public class Scpi : Protocol
	{
		private readonly Stream stream;

		public Scpi(Stream stream)
		{
			this.stream = stream;
			byte[] bytes = Encoding.ASCII.GetBytes(Terminator).ToArray();
			stream.Write(bytes, 0, bytes.Length);
		}

		public float GetAbsolutePressure(Module module) =>
			GetBarometricPressure(module) + GetPressure(module);
		
		public float GetBarometricPressure(Module module) =>
			new GetBarometricPressure(stream).Execute(module);

		public string GetManufacturer() =>
			new GetManufacturer(stream).Execute();

		public float GetMaximumPressure(Module module) =>
			new GetMaximumPressure(stream).Execute(module);

		public float GetMinimumPressure(Module module) =>
			new GetMinimumPressure(stream).Execute(module);

		public string GetName() =>
			new GetName(stream).Execute();

		public State GetOutputState(Module module) =>
			new GetOutputState(stream).Execute(module);

		public State GetOutputValveState(Module module) =>
			new GetOutputValveState(stream).Execute(module);
		
		public float GetPressure(Module module) =>
			new GetPressure(stream).Execute(module);

		public PressureEvent GetPressureEvent() =>
			new GetPressureEvent(stream).Execute();

		public string GetSerialNumber() =>
			new GetSerialNumber(stream).Execute();

		public Unit GetUnit(Module module) =>
			new GetUnit(stream).Execute(module);

		public VentingStatus GetVentingStatus(Module module) =>
			new GetVentingStatus(stream).Execute(module);

		public void SetModeToLocal() =>
			new SetModeToLocal(stream).Execute();

		public void SetOutputState(State state, Module module) =>
			new SetOutputState(stream).Execute(state, module);

		public void SetOutputValveState(State state, Module module) =>
			new SetOutputValveState(stream).Execute(state, module);
		
		public void SetPressure(float pressure, Module module) =>
			new SetPressure(stream).Execute(pressure, module);

		public void SetUnit(Unit unit, Module module) =>
			new SetUnit(stream).Execute(unit, module);

		public void StartVenting(Module module) =>
			new SetVentingStatus(stream).Execute(VentingAction.Start, module);

		public void StopVenting(Module module) =>
			new SetVentingStatus(stream).Execute(VentingAction.Abort, module);
	}
}
